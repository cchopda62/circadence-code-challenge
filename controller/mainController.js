var express = require('express');
var router = express.Router();
var battleDb = require('../utility/battleroomdb');
var mongoose = require('mongoose');
//connect to database - if it does not exist it will be created

router.get('/', function (request, response, next) {
    response.render("index");
});

router.post('/leader',async function(req, res, next){
  data = await battleDb.getBattlerooms();
  res.render("random", data);
})

router.post('/playerScore', async function(req,res,next){
  console.log("a"+req.body.pl);
  var btroom1 = [];
  var btroom2 = [];
  data = await battleDb.getBattlerooms();

  btroom1 = data[0].players;
  btroom2 = data[1].players;  
  btroom1.sort(compare);
  btroom2.sort(compare);
  var rank1 = 0;
  var rank2 = 0;
  for(var i = 0;i<btroom1.length;i++){
    if(req.body.pl == (btroom1[i].playerId).toString()){
      rank1 = i+1;  
    }
  }
  for(var i = 0;i<btroom2.length;i++){
    if(req.body.pl == (btroom2[i].playerId).toString()){
      rank2 = i+1;  
    }
  }
  console
  returnData = [{"battle":1, "rank":rank1},{"battle":2, "rank":rank2}];
  
  res.render('playerScore',returnData);
})

function compare( a, b ) {
  if ( a.score < b.score ){
    return 1;
  }
  if ( a.score > b.score ){
    return -1;
  }
  return 0;
}

router.post('/random',async function(req, res, next){
  data = await battleDb.getBattlerooms();
  for(var i=0;i<data.length;i++){
    for(var j=0;j<data[i].players.length;j++){
      var battleroom_id = data[i].battleroom_id;
      var _id =  data[i]._id
      //var playeId = data[i].players[j].playerId;
      var retrieve  = data[i].players[j].score;
      var new_score = retrieve + Math.floor(Math.random() * (500 -10+1) +10);
      var data  = await battleDb.updateScore(_id,battleroom_id,j,new_score);
      data = await battleDb.getBattlerooms();
    }
    
  }
   res.render("index");
})



module.exports = router;
