var mongoose = require('mongoose');

var schemaOptions = {
    timestamps: true,
    toJSON: {
      virtuals: true
    }
};

var battleSchema = new mongoose.Schema({
    battleroom_id: {type: Number, default: '0000'},
    players: {type: Array, default:[]},
  },schemaOptions);



module.exports = mongoose.model('battlerooms', battleSchema);
