var battleroom = require('../model/battleroom');
var battledb = {};



battledb.getBattlerooms = function () {
    return new Promise((resolve, reject) =>{
        battleroom.find({}).then(function(data) {
            resolve(data);
        }).catch(function(err) {
            console.log("Error:", err);
            return reject(err); 
        });
    });
};

battledb.updateScore = function (_id,battleroom_id,playerId, new_score){
    console.log("a" + playerId );
    return new Promise((resolve, reject) =>{
        battleroom.update({'battleroom_id':battleroom_id,'players.playerId':playerId }, {'$set': {
         'players.$.score': new_score
        }}).then(function(data) {
            resolve(data);
        }).catch(function(err) {
            console.log("Error:", err);
            return reject(err); 
        });
    });
}


module.exports = battledb;