# Circadence Engineer Recruitment Test

## About Circadence

Circadence Corporation is the market leader in next-generation cybersecurity readiness.

Our history of software advancement, multi-player game development, and a deep understanding of application optimization allows us to help develop cyber professionals who are problem-solvers, critical thinkers, and passionate about protecting and defending their organization.

Project Ares allows cyber teams from across enterprise, government, and academic institutions to automate and augment the cyber workforce by providing immersive, gamified cyber range learning environments that emulate company networks. Realistic scenarios engage teams in immersive, mission-specific virtual environments using real-world tools, network activity and a large library of authentic threat scenarios.

## Engineering Applicant Coding Challenges

Most engineering positions require an applicant to complete a coding challenge.
The coding challenges are located at <https://gitlab.com/Circadence-Public/code-challenge.>  Each application category has a corresponding test described in a file of the following format:  

{applicationCategory}.{applicationArea}.{applicationLevel}.md

E.g., the markdown file Engineer.Backend.Intern.md contains the instructions and questions for completing a challenge
    for the **Engineer**ing team
    as a **Backend** Developer
    at the **Intern** level

Each challenge will have the following sections:

- a description of the Coding Challenge
- a description of Platform Choices
- the Task Requirements that specifies minimum requirements for completion and any optional requirements
- one or more user stories that pertain to the challenge
- acceptance criteria for the user stories
- a questionaire to be completed as part of the challenge
- 


How to run code:

1. Run db file in mongodb to create backend data
2. After setting up database , node app to run the application on localhost:8080
3. If you select leaderboard button It will display all 2 battle rooms with all player id
4. if you select play a game button, then random scores will get added to each player, Its like a new round has been played.
5. And by selecting any player from dropdown you can get the rank of that player from both battle rooms.
6. Due to lack of time, As an end of semester exams and project. I made sure I complete functionality. I have not taken care of css and html in extend as its an backend














1. How long did you spend on the coding challenge?
--> 3 hours  due to exams and projects.
2. What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.
--> Definately I would like to add features like displaying every battle room score for each round.
--> I wanted my leaderboard to display by rank which was not possible due to time
--> Difference of scores between 2 players
--> Number of battle room a sinle player won
3. Explain your choice of technologies used.  Discuss any trade-offs.
--> I have used mongodb as database, Nodejs , Javascript, HTML and CSS.
4. If you were going to test this, how would you do about doing that? What would you test for?
--> All players details like id and name.
--> as scores will update continuous testing is needed.we can add some integration tools for continuous testing.
--> Making sure that scores does not get add upto max value.
--> we can use jenkins, gitlab/githu, selenium for continuous testing.

5. How would you handle a new user story to get a player's rank and median battleroom score by username?
--> I got players rank.
--> median battleroom score will easy to get by my method of updating scores, from there we can get values and find median.














Email any coding challenge related questions to: eng_challenge_help@circadence.com

    